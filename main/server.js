const express = require('express')
const bodyParser = require('body-parser')

const Sequelize = require('sequelize')
const mysql = require('mysql2/promise')

const cors = require('cors')
const moment = require('moment')

let conn;
let DB_USERNAME = 'root'
let DB_PASSWORD = 'welcome123'

mysql.createConnection({
    user: DB_USERNAME,
    password: DB_PASSWORD
})
    .then((connection) => {
        conn = connection
        return connection.query('CREATE DATABASE IF NOT EXISTS feedback_DB')
    })
    .then(() => {
        return conn.end()
    })
    .catch((err) => {
        console.warn(err.stack)
    })

const sequelize = new Sequelize('feedback_DB', DB_USERNAME, DB_PASSWORD,
    {
        dialect: 'mysql',
        dialectOptions: {
            dateStrings: true,
            typeCast: true
        }
    })

// The teacher entity is allowed to define activities, see a list of all activities that they have defined, see detailed logs of reactions to any of their activities or 
// see the reactions in real-time as they are sent by the participating students. Teachers are also able to delete any of their activities, or update them. 

// Authentication into a teacher account is made through the teacher form of the front-end part of our project.
// A teacher name must be composed of, at the very least, two names (a first name and a last name).
let Teacher = sequelize.define('teacher',
    {
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                validateMultipleWords(value) {//check if the name is composed of at least 2 words separated by one whitespace
                    let regExpMultipleWords = /\w+(?:\s\w+)+/
                    if (regExpMultipleWords.exec(value) === null) {
                        throw new Error('Teacher name is invalid, it needs to contain at least two words.')
                    }
                }
            }
        }
    })

// A teacher entity can have one or more defined activities. The access code is the primary key of the Activities table, and it is composed of 5 to 10 characters.
// Activities are created by teachers, that will have access to a form that allows them to input a description, the accessCode and a duration. 
// Duration itself is not a property of an activity, but end time is. End time is calculated as the time at which the activity is created + the specified duration.

// Activities can be accesssed by students as long as they are ongoing, this is done via the student side of the client, where students are required to specify the access 
// code of the activity that they wish to connect to.
let Activity = sequelize.define('activity',
    {
        description: {
            type: Sequelize.STRING
        },
        accessCode: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                len: [5 - 10]
            },
            primaryKey: true
        },
        endTime: {
            type: Sequelize.DATE
        }
    })

// An activity can have one or more reactions, that are sent in anonymously by the students participating in the activity. A reaction has one of four types, each 
// signifying a certain emotion (smiley, frowny, surprised, confused). 

// When students connect to an ongoing activity, they have an interface split in four quadrants, each of them associated to one of the aforementioned types of reactions 
// and indicated by an emoticon image.

// The reaction time represents the time at which the student clicks or taps on an emotion quadrant. The reaction time will also be used as a way of disconnecting students 
// from an activity, if they attempt to send a reaction after the activity end time has been reached.
let Reaction = sequelize.define('reaction',
    {
        reactionTime: {
            type: Sequelize.DATE
        },
        reactionType: {
            type: Sequelize.ENUM,
            values: ['Smiley', 'Frowny', 'Surprised', 'Confused']
        }
    })

Teacher.hasMany(Activity)
Activity.hasMany(Reaction)

const app = express()


app.use(cors())
app.use(bodyParser.json())

///////////////////////////////////////////////////////////////////////////////// ENDPOINTS ///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////// GET ENDPOINTS ////////////////////////////////////////////////////////////////////////////////

// Returns all teachers, or a message that shows that no teachers exist.
app.get('/teachers', async (req, res, next) => {
    try {
        let teachers = await Teacher.findAll()
        if (teachers.length != 0) {
            res.status(200).json(teachers)
        }
        else {
            res.status(404).json({ message: 'No teachers found.' })
        }
    }
    catch (err) {
        next(err)
    }
})

// Returns all activities, or a message that shows that no activities exist.
app.get('/activities', async (req, res, next) => {
    try {
        let activities = await Activity.findAll()
        if (activities.length != 0) {
            res.status(200).json(activities)
        }
        else {
            res.status(404).json({ message: 'No activities found.' })
        }
    }
    catch (err) {
        next(err)
    }
})

// Returns all reactions, or a message that shows that no reactions exist.
app.get('/reactions', async (req, res, next) => {
    try {
        let reactions = await Reaction.findAll()
        if (reactions.length != 0) {
            res.status(200).json(reactions)
        }
        else {
            res.status(404).json({ message: 'No reactions found.' })
        }
    }
    catch (err) {
        next(err)
    }
})

// Returns OK if an activity has not ended. Used in the Student component.
app.get('/activities/:accessCode', async (req, res, next) => {
    try {
        let activity = await Activity.findByPk(req.params.accessCode)
        if (activity) {
            if (checkActivityTimeValid(activity.endTime)) {
                res.status(200).json({ message: 'OK' })
            }
            else {
                res.status(404).json({ message: 'Activity has ended.' })
            }
        }
        else {
            res.status(404).json({ message: 'Activity does not exist.' })
        }
    }
    catch (err) {
        next(err)
    }
})

//Returns a list of the activities that belong to the specified teacher.
app.get('/teachers/:tuser/activities', async (req, res, next) => {
    try {
        let teacher = await Teacher.findByPk(req.params.tuser, {
            include: [Activity] //join the tables
        })
        if (teacher) {
            if (teacher.activities) {
                res.status(200).json(teacher.activities)
            }
            else {
                res.status(404).json({ message: 'The teacher has no activities.' })
            }
        }
        else {
            res.status(404).json({ message: 'Teacher does not exist.' })
        }
    }
    catch (err) {
        next(err)
    }
})


// Returns all reactions that belong to a certain activity.
app.get('/activities/:accessCode/reactions', async (req, res, next) => {
    try {
        let activity = await Activity.findByPk(req.params.accessCode)
        if (activity) {
            let reactions = await Reaction.findAll({ where: { activityAccessCode: req.params.accessCode } })

            if (reactions.length != 0) {
                res.status(200).json(reactions)
            }
            else {
                res.status(404).json({ message: 'No reactions found.' })
            }
        }
        else {
            res.status(404).json({ message: 'Activity does not exist.' })
        }
    } catch (err) {
        next(err)
    }
})

// Checks if a teacher that has the specified id and password exists. If so, 200 status and the json message "OK" are returned. Used for authentication purposes.
app.get('/teachers/:tuser&:tpassword', async (req, res, next) => {
    try {
        let teacher = await Teacher.findByPk(req.params.tuser)
        if (teacher) {
            if (teacher.password == req.params.tpassword) {
                res.status(200).json({ message: "OK" })
            }
            else {
                res.status(404).json({ message: "The specified username and password combination doesn't exist" })
            }
        }
        else { res.status(404).json({ message: "The specified username and password combination doesn't exist" }) }
    }
    catch (err) {
        next(err)
    }
})

///////////////////////////////////////////////////////////////////////////////// POST ENDPOINTS ////////////////////////////////////////////////////////////////////////////////

// Synchronizes the database, permits dropping tables with foreign key constraints as well.
app.post('/sync', async (req, res, next) => {
    try {
        await sequelize.query('SET FOREIGN_KEY_CHECKS = 0').then(function () {
            sequelize.sync({ force: true }).then(function () {
                sequelize.query('SET FOREIGN_KEY_CHECKS = 1')
            })
        })
        res.status(201).json({ message: 'DB created' })
    }
    catch (err) {
        next(err)
    }
})

// Creates a teacher if there is not a record with the same username in the database.
app.post('/teachers', async (req, res, next) => {
    try {
        let teacher = req.body
        let checkTeacher = await Teacher.findByPk(teacher.username)
        if (checkTeacher) {
            res.status(409).json({ message: 'A teacher that has the specified username already exists.' })
        }
        else {
            await Teacher.create(teacher)
            res.status(201).json({ message: 'Created teacher' })
        }
    }
    catch (err) {
        next(err)
    }
})

// Creates an activity that belongs to a teacher, if said teacher exists. 
// Also signals if an activity with the primary key specified in the request body already exists in the database.
app.post('/teachers/:username/activity', async (req, res, next) => {
    try {
        let teacher = await Teacher.findByPk(req.params.username)
        if (teacher) {
            let activity = req.body
            let checkActivity = await Activity.findByPk(activity.accessCode)
            if (checkActivity) {
                res.status(409).json({ message: 'An activity that has the specified access code already exists.' })
            }
            else {
                activity.teacherUsername = teacher.username
                await Activity.create(activity)
                res.status(201).json({ message: 'Created activity' })
            }
        }
        else {
            res.status(404).json({ message: 'Teacher not found. No activity was created' })
        }
    } catch (err) {
        next(err)
    }
})

// Checks if an activity exists and is ongoing, and if so adds the reaction specified in the request body
// to said activity (Smiley, Frowny, Surprised, Confused). 
app.post('/activities/:accessCode/reaction', async (req, res, next) => {
    try {
        let activity = await Activity.findByPk(req.params.accessCode)
        if (activity) {
            let reaction = req.body
            reactionTime = moment.utc().format('YYYY-MM-DD HH:mm:ss')
            if (reactionTime < activity.endTime) {
                reaction.activityAccessCode = activity.accessCode
                await Reaction.create(reaction)
                res.status(201).json({ message: 'Created reaction.' })
                io.emit(`newReaction${activity.accessCode}`, reaction)
            }
            else {
                res.status(405).json({ message: 'Activity has ended.' })
            }
        }
        else {
            res.status(404).json({ message: 'Activity not found.' })
        }
    } catch (err) {
        next(err)
    }
})

///////////////////////////////////////////////////////////////////////////////// PUT ENDPOINTS ////////////////////////////////////////////////////////////////////////////////


// Updates the teacher that has the username specified in the parameters, if it exists, with the new values in the request body.
app.put('/teachers/:username', async (req, res, next) => {
    try {
        let teacher = await Teacher.findByPk(req.params.username)
        if (teacher) {
            await teacher.update(req.body, {
                fields: ['password', 'name']
            })
            res.status(200).json({ message: 'Teacher updated.' })
        }
        else {
            res.status(404).json({ message: 'Teacher does not exist.' })
        }
    } catch (err) {
        next(err)
    }
})

// Updates the activity that has the access code specified in the parameters. 
app.put('/activities/:accessCode', async (req, res, next) => {
    try {
        let activity = await Activity.findByPk(req.params.accessCode)
        if (activity) {
            await activity.update(req.body, {
                fields: ['description', 'endTime']
            })
            res.status(200).json({ message: 'Activity updated.' })
        }
        else {
            res.status(404).json({ message: 'Activity does not exist.' })
        }
    }
    catch (err) {
        next(err)
    }
})

/////////////////////////////////////////////////////////////////////////////// DELETE ENDPOINTS ////////////////////////////////////////////////////////////////////////////////

// Deletes a teacher based on the username received. Also deletes all activities belonging to the teacher and all the reactions that belong to said activities.
app.delete('/teachers/:username', async (req, res, next) => {
    try {
        let teacher = await Teacher.findByPk(req.params.username)

        if (teacher) {
            let activities = await teacher.getActivities()
            for (const activity of activities) {
                await Reaction.destroy({ where: { activityAccessCode: activity.accessCode } })
                await Activity.destroy({ where: { accessCode: activity.accessCode } })
            }
            await Teacher.destroy({ where: { username: teacher.username } })
            res.status(200).json({ message: 'Deleted teacher, their activities and the activities\' associated reactions.' })
        } else {
            res.status(404).json({ message: 'Teacher does not exist.' })

        }
    }
    catch (err) {
        next(err)
    }
})

// Deletes an activity if it exists. It also deletes all reactions that belong to the activity.
app.delete('/activities/:accessCode', async (req, res, next) => {
    try {

        let activity = await Activity.findByPk(req.params.accessCode)

        if (activity) {
            await Reaction.destroy({ where: { activityAccessCode: activity.accessCode } })
            await Activity.destroy({ where: { accessCode: activity.accessCode } })
            res.status(200).json({ message: 'Deleted activity and its reactions.' })
        } else {
            res.status(404).json({ message: 'Activity does not exist.' })
        }
    }
    catch (err) {
        next(err)

    }
})

////////////////////////////////////////////////////////////////////////// END OF ENDPOINTS ///////////////////////////////////////////////////////////////////////////////////

app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).json({ message: 'Server failed successfully!' })
})

// Function that finds out if the activity has ended or is ongoing
function checkActivityTimeValid(activityEndTime) {

    let date = moment().utc().format("YYYY-MM-DD HH:mm:ss")
    //Return true if the activity's end time is in the future, return false otherwise
    if (date < activityEndTime) {
        return true;
    }
    else {
        return false;
    }
}

const server = app.listen(8080)
const io = require('socket.io').listen(server, { origins: '*:*' })

io.on('connection', (socket) => {
    console.log("YAY! A NEW CLIENT!")
})