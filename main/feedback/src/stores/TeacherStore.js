import { EventEmitter } from 'fbemitter'

const SERVER = 'http://localhost:8080'

class TeacherStore {
    constructor() {
        this.emitter = new EventEmitter()
    }

    async getTeacherExists(teacherUsername, teacherPassword) {
        try {
            let response = await fetch(`${SERVER}/teachers/${teacherUsername}&${teacherPassword}`)
            let data = await response.json()

            if (data.message === "OK") {
                this.emitter.emit('GET_TEACHER_LOGIN_SUCCESS')
            }
            else {
                this.emitter.emit('GET_TEACHER_LOGIN_FAIL')
            }
        }
        catch (err) {
            this.emitter.emit('GET_TEACHER_LOGIN_ERROR')
        }
    }

    async addTeacher(teacherName, teacherUsername, teacherPassword) {

        let teacher = {
            name: teacherName,
            username: teacherUsername,
            password: teacherPassword
        }

        let request = await fetch(`${SERVER}/teachers`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(teacher)
            })

        let data = await request.json()

        if (data.message === 'A teacher that has the specified username already exists.') {
            this.emitter.emit("ADD_TEACHER_CONFLICT")
        }
        else if (data.message === 'Created teacher') {
            this.emitter.emit("ADD_TEACHER_SUCCESS")
        }
    }

}

export default TeacherStore