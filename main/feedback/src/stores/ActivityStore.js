import { EventEmitter } from 'fbemitter'
import moment from 'moment'

const SERVER = 'http://localhost:8080'

class ActivityStore {
    constructor() {
        this.emitter = new EventEmitter()
    }

    async getActivity(activityCode) {
        try {
            let response = await fetch(`${SERVER}/activities/${activityCode}`)
            let data = await response.json()

            if (data.message === "OK") {
                this.emitter.emit('GET_ACTIVITY_SUCCESS')
            }
            else {
                this.emitter.emit('GET_ACTIVITY_FAIL')
            }
        }
        catch (err) {
            this.emitter.emit('GET_ACTIVITY_ERROR')
        }
    }

    //Get the activities of a teacher. The original UTC time of the server database is converted to the local time of the client.
    async getActivities(teacherUsername) {
        try {
            let request = await fetch(`${SERVER}/teachers/${teacherUsername}/activities`)
            let response = await request.json()
            for (let activity of response) {
                activity.endTime = moment.utc(activity.endTime).local().format('YYYY-MM-DD HH:mm:ss')
            }
            return response
        }
        catch (err) {
            this.emitter.emit('GET_ACTIVITIES_ERROR')
        }
    }

    //Get the reactions of an activity. The original UTC time of the server database is converted to the local time of the client.
    async getReactions(accessCode) {
        try {
            let request = await fetch(`${SERVER}/activities/${accessCode}/reactions`)
            let response = await request.json()
            let offset = moment().utcOffset()
            for (let reaction of response) {
                reaction.reactionTime = moment.utc(reaction.reactionTime).utcOffset(offset).format('YYYY-MM-DD HH:mm:ss')
            }
            return response
        }
        catch (err) {
            this.emitter.emit('GET_REACTIONS_ERROR')
        }
    }

    //Sends a reaction with a reactionTime equal to the current local time. The time is converted to UTC in the server.js endpoint
    async sendReaction(reactionType, activityCode) {

        let reactionTime = moment.utc().local().format('YYYY-MM-DD HH:mm:ss')
        let reaction = {
            reactionTime: reactionTime,
            reactionType: reactionType
        }

        let request = await fetch(`${SERVER}/activities/${activityCode}/reaction`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(reaction)
            })

        let data = await request.json()

        if (data.message === 'Activity has ended.' || data.message === 'Activity not found.') {
            this.emitter.emit('SEND_REACTION_FAIL')
        }
    }

    //An activity is added. The endTime received from the client is transformed into UTC time to be inserted into the database.
    async addActivity(username, accessCode, description, endTime) {
        endTime = moment.utc(endTime).format('YYYY-MM-DD HH:mm:ss')
        let activity = {
            accessCode: accessCode,
            description: description,
            endTime: endTime
        }

        let request = await fetch(`${SERVER}/teachers/${username}/activity`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(activity)
            })

        let data = await request.json()

        if (data.message === 'An activity that has the specified access code already exists.') {
            this.emitter.emit("ADD_ACTIVITY_CONFLICT")
        }
        else if (data.message === 'Created activity') {
            this.emitter.emit("ADD_ACTIVITY_SUCCESS")
        }
    }

    async updateActivity(accessCode, description, endTime) {
        let updatedFields = {
            description: description,
            endTime: endTime
        }
        let request = await fetch(`${SERVER}/activities/${accessCode}`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(updatedFields)
            })

        await request.json()
    }

    async deleteActivity(accessCode) {
        let request = await fetch(`${SERVER}/activities/${accessCode}`,
            {
                method: 'DELETE'
            })

        await request.json()
    }

}

export default ActivityStore