import React, { Component } from 'react'
import ReactDataGrid from 'react-data-grid'
import ActivityStore from '../stores/ActivityStore'
import Button from 'react-bootstrap/Button'
import '../styles/ReactionsLog.css'
import io from 'socket.io-client'
import Smiley from '../media/Smiley.png'
import Frown from '../media/Frown.png'
import Surprised from '../media/Surprised.png'
import Confused from '../media/Confused.png'
import Pop from '../media/pop.mp3'

class ReactionsLog extends Component {
    constructor(props) {
        super(props)

        this.state = {
            reactions: undefined
        }
        this.activityStore = new ActivityStore()
        this.clickBackButton = this.clickBackButton.bind(this)
        this.updateReactions = this.updateReactions.bind(this)
        this.scrollToRow = this.scrollToRow.bind(this)
        this.pop = new Audio(Pop)
        this.buttonBack = <Button className="btnBack" variant="primary" onClick={this.clickBackButton}>Go back</Button>

        this.activityStore.getReactions(this.props.accessCode).then(result => {
            this.setState({ reactions: result })
        })

        this.ReactionTypeFormatter = ({ row }) => {
            let imageSource

            switch (row.reactionType) {
                case "Smiley":
                    imageSource = Smiley
                    break
                case "Frowny":
                    imageSource = Frown
                    break
                case "Surprised":
                    imageSource = Surprised
                    break
                case "Confused":
                    imageSource = Confused
                    break
                default:
            }

            return <img src={imageSource} label={`${imageSource}`} height="200px" width="200px" alt={imageSource} />
        }

        this.columns = [
            { key: 'reactionType', name: 'Reaction Type' },
            { key: 'reactionTime', name: 'Reaction Time' },
            { key: 'reactionImage', name: 'Reaction Image', formatter: this.ReactionTypeFormatter }
        ]
    }


    componentDidMount() {
        this.socket = io("http://localhost:8080")
        this.socket.on('connect', function () { })

        this.socket.on(`newReaction${this.props.accessCode}`, reaction => this.updateReactions(reaction))
    }

    componentWillUnmount() {
        this.socket.disconnect()
        this.props.setHome()
    }

    render() {
        return this.state.reactions === undefined ? <div>{this.buttonBack}<p>No reactions found.</p></div>
            : <div>{this.buttonBack}
                <ReactDataGrid
                    ref={(g) => { this.grid = g }}
                    columns={this.columns}
                    rowGetter={i => this.state.reactions[i]}
                    rowsCount={this.state.reactions.length}
                    rowHeight={200}
                    headerRowHeight={40}
                    minHeight={700}
                />
            </div>
    }

    updateReactions(reaction) {
        let completedReactions
        if (this.state.reactions) {
            completedReactions = this.state.reactions
        }
        else {
            completedReactions = []
        }
        completedReactions.push(reaction)
        this.setState({
            reactions: completedReactions
        })
        this.scrollToRow(this.state.reactions.length - 1)
        this.pop.currentTime = 0
        this.pop.play()
    }

    clickBackButton() {
        this.props.setHome()
    }

    scrollToRow(idx) {
        console.log(this.grid)
        let top = 200 * idx
        let gridCanvas = this.grid.getDataGridDOMNode().querySelector('.react-grid-Canvas')
        gridCanvas.scrollTop = top
    }
}

export default ReactionsLog