import React, { Component } from 'react'
import Button from 'react-bootstrap/Button'
import Student from './Student'
import TeacherLogin from './TeacherLogin'
import Teacher from './Teacher'
import '../styles/App.css'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userTypeToggled: false,
      teacher: null
    }
    this.userPortal = null
    this.onUserChange = this.onUserChange.bind(this)
    this.changeTeacher = this.changeTeacher.bind(this)
  }

  render() {
    return <div id="base">
      <div id="top">
        <Button className="btnChoose" variant="primary" value={"Student"} onClick={(e) => this.onUserChange(e, "value")}>Student? Click here!</Button>
        <Button className="btnChoose" variant="info" value={"Teacher"} onClick={(e) => this.onUserChange(e, "value")}>Teacher login</Button>
      </div>

      <div>
        {(this.state.userTypeToggled) ? this.userPortal : ''}
      </div>
    </div>
  }

  onUserChange(e) {
    switch (e.target.value) {
      case "Student":
        this.userPortal = <Student />
        break;
      case "Teacher":
        this.userPortal = this.state.teacher == null ? <TeacherLogin changeTeacher={this.changeTeacher} />
          : <Teacher username={this.state.teacher} logOut={this.changeTeacher} />
        break;
      default:
        this.userPortal = null
    }
    this.setState({ userTypeToggled: true })
  }

  changeTeacher(teacher) {
    this.setState({ teacher: teacher })
    //if we send null via the parameter, we will log out the teacher and re-render this component.
    if (teacher == null) {
      this.setState({ userTypeToggled: false })
    }
  }

}

export default App;
