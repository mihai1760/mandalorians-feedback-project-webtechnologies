import React, { Component } from 'react'
import '../styles/Form.css'
import Form from 'react-bootstrap/Form'
import FormGroup from 'react-bootstrap/FormGroup'
import FormControl from 'react-bootstrap/FormControl'
import FormLabel from 'react-bootstrap/FormLabel'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Activity from './Activity'
import ActivityStore from '../stores/ActivityStore'

class Student extends Component {
    constructor() {
        super()

        this.state = {
            activityEntered: false,
            accessCode: '',
            showModal: false
        }
        this.activityStore = new ActivityStore()
        this.activityStore.emitter.addListener('GET_ACTIVITY_SUCCESS', () => {
            this.setState({ activityEntered: true })
        })
        this.activityStore.emitter.addListener('GET_ACTIVITY_FAIL', () => {
            this.showModal()
        })
        this.enterActivity = this.enterActivity.bind(this)
        this.exitActivity = this.exitActivity.bind(this)
        this.changeAccessCode = this.changeAccessCode.bind(this)
        this.showModal = this.showModal.bind(this)
        this.hideModal = this.hideModal.bind(this)

        this.formActivity = <div className="divForm"><h3>Access an ongoing activity</h3>
            <Form onSubmit={this.enterActivity}>
                <FormGroup controlId="accessCode">
                    <FormLabel>Activity code</FormLabel>
                    <FormControl type="text" placeholder="i.e: 5AKFJ" onChange={this.changeAccessCode} />
                </FormGroup>
                <Button variant="primary" type="submit">Connect</Button>
            </Form>
        </div>
    }

    changeAccessCode(e) {
        this.setState({ accessCode: e.target.value })
    }

    render() {
        return (this.state.activityEntered) ? <Activity accessCode={this.state.accessCode} exitActivity={this.exitActivity} />
            : <div>
                {this.formActivity}
                <Modal
                    show={this.state.showModal}
                    onHide={this.hideModal}
                    size="lg"
                    centered >
                    <Modal.Header>
                        <Modal.Title>Ongoing activity not found</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Failed to find an ongoing activity that matched the submitted code. Please try again.
                </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={this.hideModal}>Exit</Button>
                    </Modal.Footer>
                </Modal>
            </div>
    }

    enterActivity(e) {
        e.preventDefault()
        this.activityStore.getActivity(this.state.accessCode)
    }

    exitActivity() {
        this.setState({ activityEntered: false })
    }

    showModal() {
        this.setState({ showModal: true })
    }

    hideModal() {
        this.setState({ showModal: false })
    }
}

export default Student