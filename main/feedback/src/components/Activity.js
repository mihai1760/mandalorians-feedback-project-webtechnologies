import React, { Component } from 'react'
import Smiley from '../media/Smiley.png'
import Frown from '../media/Frown.png'
import Surprised from '../media/Surprised.png'
import Confused from '../media/Confused.png'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import '../styles/Activity.css'
import ActivityStore from '../stores/ActivityStore'

class Activity extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showModal: false
        }

        this.activityStore = new ActivityStore()
        this.activityStore.emitter.addListener('SEND_REACTION_FAIL', () => {
            this.showModal()
        })
        this.sendReaction = this.sendReaction.bind(this)
        this.showModal = this.showModal.bind(this)
        this.hideModal = this.hideModal.bind(this)

    }

    render() {

        return <div id="reactionsGrid">
            <div id="smiley">
                <img src={Smiley} alt="Smiley" onClick={(e) => this.sendReaction(e, "alt")} />
            </div>
            <div id="frowny">
                <img src={Frown} alt="Frowny" onClick={(e) => this.sendReaction(e, "alt")} />
            </div>
            <div id="surprised">
                <img src={Surprised} alt="Surprised" onClick={(e) => this.sendReaction(e, "alt")} />
            </div>
            <div id="confused">
                <img src={Confused} alt="Confused" onClick={(e) => this.sendReaction(e, "alt")} />
            </div>

            <Modal
                show={this.state.showModal}
                onHide={this.hideModal}
                size="lg"
                centered >
                <Modal.Header>
                    <Modal.Title>Activity has ended</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    The activity has ended. We hope you have enjoyed your stay!
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={this.hideModal}>Exit</Button>
                </Modal.Footer>
            </Modal>
        </div>
    }

    showModal() {
        this.setState({ showModal: true })
    }

    hideModal() {
        this.setState({ showModal: false })
        this.props.exitActivity()
    }

    sendReaction(e) {
        this.activityStore.sendReaction(e.target.alt, this.props.accessCode)
    }
}

export default Activity