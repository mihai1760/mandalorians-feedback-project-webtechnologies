import React, { Component } from 'react'
import '../styles/Form.css'
import FormControl from 'react-bootstrap/FormControl'
import FormLabel from 'react-bootstrap/FormLabel'
import Form from 'react-bootstrap/Form'
import FormGroup from 'react-bootstrap/FormGroup'
import Modal from 'react-bootstrap/Modal'
import Teacher from './Teacher'
import TeacherStore from '../stores/TeacherStore'

import Button from 'react-bootstrap/Button'

class TeacherLogin extends Component {
    constructor(props) {
        super(props)

        this.state = {
            teacherCorrect: false,
            username: '',
            password: '',
            showNotFoundModal: false,
            showSuccessModal: false,
            showFailModal: false
        }
        this.loginTeacher = this.loginTeacher.bind(this)
        this.addTeacher = this.addTeacher.bind(this)
        this.toggleTeacher = this.toggleTeacher.bind(this)
        this.showModal = this.showModal.bind(this)
        this.hideNotFoundModal = this.hideNotFoundModal.bind(this)
        this.hideSuccessModal = this.hideSuccessModal.bind(this)
        this.hideFailModal = this.hideFailModal.bind(this)


        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }

        this.teacherStore = new TeacherStore()
        this.teacherStore.emitter.addListener('GET_TEACHER_LOGIN_SUCCESS', () => {
            this.toggleTeacher()
            this.props.changeTeacher(this.state.username)
        })
        this.teacherStore.emitter.addListener('GET_TEACHER_LOGIN_FAIL', () => {
            this.showModal('not found')
        })
        this.teacherStore.emitter.addListener('ADD_TEACHER_SUCCESS', () => {
            this.showModal('success')
        })
        this.teacherStore.emitter.addListener('ADD_TEACHER_CONFLICT', () => {
            this.showModal('fail')
        })

        this.formActivity = <div className="divForm"><h3>Login into a teacher account</h3>
            <Form onSubmit={this.loginTeacher}>
                <FormGroup controlId="username">
                    <FormLabel>Username</FormLabel>
                    <FormControl type="text" name="username" onChange={this.handleChange} placeholder=" " />
                </FormGroup>
                <FormGroup controlId="password">
                    <FormLabel>Password</FormLabel>
                    <FormControl type="password" name="password" onChange={this.handleChange} placeholder="Remember to check Caps Lock"></FormControl>
                </FormGroup>
                <Button variant="primary" type="submit">Connect</Button>
            </Form>
        </div>

        this.addTeacherForm = <div className="divForm"><h3>Create teacher account</h3>
            <Form onSubmit={this.addTeacher}>
                <FormGroup controlId="nameAdd">
                    <FormLabel>Teacher name</FormLabel>
                    <FormControl type="text" name="nameAdd" onChange={this.handleChange} placeholder="At least a first name and a last name." />
                </FormGroup>
                <FormGroup controlId="usernameAdd">
                    <FormLabel>Teacher username</FormLabel>
                    <FormControl type="text" name="usernameAdd" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup controlId="passwordAdd">
                    <FormLabel>Teacher password</FormLabel>
                    <FormControl type="password" name="passwordAdd" onChange={this.handleChange} placeholder="Remember to check Caps Lock"></FormControl>
                </FormGroup>
                <Button variant="primary" type="submit">Create account</Button>
            </Form>
        </div>
    }

    //If the teacherCorrect variable is true, the user is redirected into a teacher home page. Otherwise, the base login page with two forms appears
    render() {
        return (this.state.teacherCorrect) ? <Teacher username={this.state.username} logOut={this.props.changeTeacher} />
            : <div>
                {this.formActivity} {this.addTeacherForm}
                <Modal
                    show={this.state.showNotFoundModal}
                    onHide={this.hideNotFoundModal}
                    size="lg"
                    centered >
                    <Modal.Header>
                        <Modal.Title>No teacher found</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        No teacher account that matched the submitted credentials was found. Please try again.
                </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={this.hideNotFoundModal}>Exit</Button>
                    </Modal.Footer>
                </Modal>

                <Modal
                    show={this.state.showSuccessModal}
                    onHide={this.hideSuccessModal}
                    size="lg"
                    centered >
                    <Modal.Header>
                        <Modal.Title>Teacher account created</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Teacher account successfully created!
                </Modal.Body>
                    <Modal.Footer>
                        <Button variant="success" onClick={this.hideSuccessModal}>Exit</Button>
                    </Modal.Footer>
                </Modal>

                <Modal
                    show={this.state.showFailModal}
                    onHide={this.hideFailModal}
                    size="lg"
                    centered >
                    <Modal.Header>
                        <Modal.Title>Username already exists</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Teacher account with the specified username already exists. Please specify a different one.
                </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={this.hideFailModal}>Exit</Button>
                    </Modal.Footer>
                </Modal>
            </div>
    }

    toggleTeacher() {
        let teacherCorrect = !this.state.teacherCorrect
        this.setState({ teacherCorrect: teacherCorrect })
    }

    //check if a teacher account with the specified credentials exists
    loginTeacher(e) {
        e.preventDefault()
        this.teacherStore.getTeacherExists(this.state.username, this.state.password)
    }

    addTeacher(e) {
        e.preventDefault()
        this.teacherStore.addTeacher(this.state.nameAdd, this.state.usernameAdd, this.state.passwordAdd)
    }

    showModal(type) {
        switch (type) {
            case 'not found':
                this.setState({ showNotFoundModal: true })
                break
            case 'success':
                this.setState({ showSuccessModal: true })
                break
            case 'fail':
                this.setState({ showFailModal: true })
                break
            default:
        }
    }

    hideNotFoundModal() {
        this.setState({ showNotFoundModal: false })
    }

    hideSuccessModal() {
        this.setState({ showSuccessModal: false })
    }

    hideFailModal() {
        this.setState({ showFailModal: false })
    }

}

export default TeacherLogin