import React, { Component } from 'react'
import ReactDataGrid from 'react-data-grid'
import ActivityStore from '../stores/ActivityStore'
import ReactionsLog from './ReactionsLog'
import { Toolbar } from 'react-data-grid-addons'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import { Form, FormLabel, FormControl, FormGroup } from 'react-bootstrap'
import moment from 'moment'

class Teacher extends Component {
    constructor(props) {
        super(props)

        this.state = {
            current: '',
            activities: undefined,
            showAddModal: false,
            selectedAccessCode: undefined
        }

        this.activityStore = new ActivityStore()
        this.addActivity = this.addActivity.bind(this)
        this.setHome = this.setHome.bind(this)
        this.setReactions = this.setReactions.bind(this)
        this.deleteActivity = this.deleteActivity.bind(this)
        this.getCellActions = this.getCellActions.bind(this)
        this.onRowClicked = this.onRowClicked.bind(this)
        this.onGridRowsUpdated = this.onGridRowsUpdated.bind(this)
        this.logOut = this.logOut.bind(this)

        this.activityStore.emitter.addListener('ADD_ACTIVITY_SUCCESS', () => {
            this.toggleModal()
        })

        this.activityStore.emitter.addListener('ADD_ACTIVITY_CONFLICT', () => {
            alert('An activity with the specified access code already exists. Please input a different one.')
        })
    }

    componentDidMount() {
        this.columns = [
            { key: 'accessCode', name: 'Access Code' },
            { key: 'description', name: 'Description (editable)', editable: true },
            { key: 'endTime', name: 'End Time (editable)', editable: true }
        ]
        this.activityStore.getActivities(this.props.username).then(result => {
            this.setState({ activities: result })
            this.setState({ current: 'home' })
        })
        this.toggleModal = this.toggleModal.bind(this)

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }

        //Actions are small, button-like interactive markers specific to react data grid.
        //Below are declared two sets of actions. The first one is meant for the first cell of a row, and the second one for the other two cells. 
        this.activityActions = [
            {
                icon: <i className="fas fa-link" />,
                actions: [
                    {
                        text: "View reactions",
                        callback: this.setReactions
                    }
                ]
            },

            {
                icon: <i className="fas fa-trash-alt" />,
                actions: [
                    {
                        text: "Delete activity",
                        callback: this.deleteActivity
                    }
                ]
            }
        ]

        this.pencilAction = [
            {
                icon: <i className="fas fa-pencil-alt" />
            }
        ]
    }

    getCellActions(column) {
        const cellActions = {
            accessCode: this.activityActions,
            description: this.pencilAction,
            endTime: this.pencilAction
        }
        return cellActions[column.key]
    }

    //We set the selected access code to be able to find out on which row the action to delete or view reactions should be done.
    onRowClicked(rowIdx) {
        if (rowIdx !== -1) {
            let accessCode = this.state.activities[rowIdx].accessCode
            this.setState({ selectedAccessCode: accessCode })
        }
    }

    //React data grid supports the ability to update multiple rows simultaneously. 
    onGridRowsUpdated = ({ fromRow, toRow, updated }) => {
        const activities = this.state.activities.slice()
        for (let i = fromRow; i <= toRow; i++) {
            activities[i] = { ...activities[i], ...updated }
            this.activityStore.updateActivity(activities[i].accessCode, activities[i].description, activities[i].endTime)
        }
        this.setState({ activities: activities })
    }


    render() {
        switch (this.state.current) {
            case "home":
                return <div><ReactDataGrid
                    columns={this.columns}
                    rowGetter={i => this.state.activities[i]}
                    rowsCount={this.state.activities.length !== undefined ? this.state.activities.length : 0}
                    minHeight={700}
                    toolbar={<Toolbar>
                        <Button variant="primary" type="submit" onClick={this.toggleModal}>Add new activity</Button>
                        <Button variant="primary" type="submit" onClick={this.logOut}>Log out</Button>
                    </Toolbar>}
                    getCellActions={this.getCellActions}
                    onRowClick={this.onRowClicked}
                    enableCellSelect={true}
                    onGridRowsUpdated={this.onGridRowsUpdated}
                />

                    <Modal
                        show={this.state.showAddModal}
                        onHide={this.toggleModal}
                        size="lg"
                        centered >
                        <Modal.Header>
                            <Modal.Title>Add an activity</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form onSubmit={this.addActivity}>
                                <FormGroup>
                                    <FormLabel>Activity access code</FormLabel>
                                    <FormControl type="text" placeholder="i.e: 5AKFJ (5-10 characters)" name="accessCode" onChange={this.handleChange} />
                                    <FormLabel>Activity description</FormLabel>
                                    <FormControl type="text" name="description" onChange={this.handleChange} />
                                    <FormLabel>Activity end time, format: YYYY-MM-DD HH:mm:ss</FormLabel>
                                    <FormControl type="text" name="endTime"
                                        defaultValue={moment().local().add(1, 'hour').format("YYYY-MM-DD HH:mm:ss")}
                                        onChange={this.handleChange} />
                                </FormGroup>
                                <Button variant="success" type="submit">Add activity</Button>
                                <Button variant="secondary" onClick={this.toggleModal}>Cancel</Button>
                            </Form>
                        </Modal.Body>
                    </Modal>

                </div>
            case "reactions":
                return <ReactionsLog setHome={this.setHome} accessCode={this.state.selectedAccessCode} />
            default:
                return <p>Loading...</p>
        }
    }

    //The default end time for an activity, generated when a teacher clicks the "Add Activity" button, is the current local time + 1 hour.
    toggleModal() {
        let defaultTime = moment().local().add(1, 'hour').format("YYYY-MM-DD HH:mm:ss")
        this.setState({endTime : defaultTime})
        let value = !this.state.showAddModal
        this.setState({ showAddModal: value })
    }

    logOut() {
        this.props.logOut(null)
    }

    addActivity(e) {
        e.preventDefault()

        this.activityStore.addActivity(this.props.username,
            this.state.accessCode,
            this.state.description,
            this.state.endTime).then(async () => {
                let fetchedActivities = await this.activityStore.getActivities(this.props.username)
                this.setState({ activities: fetchedActivities })
            })
    }

    deleteActivity() {
        this.activityStore.deleteActivity(this.state.selectedAccessCode).then(async () => {
            let activities = await this.activityStore.getActivities(this.props.username)
            this.setState({ activities: activities })
        })
    }

    setHome() {
        this.setState({ current: "home" })
    }
    setReactions() {
        this.setState({ current: "reactions" })
    }

}

export default Teacher